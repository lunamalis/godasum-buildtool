namespace GodasumBuildtool.Activities {

	using System;
	using System.IO;
	using System.Diagnostics;

	public static class BuildDLL
	{

		public static void Run(Buildtool bt, string configuration) {

			_build_dll(bt, configuration);

			var mi = bt.ModInfo;
			var src_path = Path.Combine(bt.TargetPath, ".mono", "temp", "bin", configuration, $"{mi.NamespaceName}.dll");
			var dest_dir = Path.Combine(bt.TargetPath, "build", $"{mi.FullName}", "mono");
			var dest_path = Path.Combine(dest_dir, $"{mi.NamespaceName}.dll");

			Directory.CreateDirectory(dest_dir);
			File.Copy(src_path, dest_path, overwrite: true);

		}

		private static void _build_dll(Buildtool bt, string configuration) {

			var dotnettools_path = bt.Config.DotnetToolsPath;

			Console.WriteLine($"dotnet: {dotnettools_path}");

			var process = new Process();
			var start_info = process.StartInfo;

			start_info.UseShellExecute = false;
			start_info.RedirectStandardOutput = true;
			start_info.RedirectStandardError = true;
			start_info.CreateNoWindow = true;

			start_info.WorkingDirectory = bt.TargetPath;

			start_info.FileName = dotnettools_path;

			start_info.Arguments = $"build --configuration {configuration}";

			process.Start();
			process.WaitForExit();

			Console.WriteLine($"STDOUT:\n{process.StandardOutput.ReadToEnd()}\n");
			Console.WriteLine($"STDERR:\n{process.StandardError.ReadToEnd()}\n");

		}

	}
}
