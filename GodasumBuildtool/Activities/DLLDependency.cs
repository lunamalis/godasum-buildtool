namespace GodasumBuildtool.Activities {
	using System;
	using System.IO;

	public static class DLLDependency
	{

		public static void GetAll(Buildtool bt, string configuration) {
			var mono_source_dir = Path.Combine(bt.TargetPath, ".mono", "temp", "bin", configuration);
			if (!Directory.Exists(mono_source_dir)) {
				throw new DirectoryNotFoundException($"Could not transfer mod dependency DLLs because they do not exist at {mono_source_dir}");
			}

			var fullname = bt.ModInfo.FullName;

			var source_dir = Path.Combine(bt.TargetPath, "build", fullname, "mono");

			_copy(mono_source_dir, source_dir);

			var appdata_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);

			var godasum_dir = Path.Combine(appdata_path, "Godasum");
			if (!Directory.Exists(godasum_dir)) {
				throw new DirectoryNotFoundException($"Godasum doesn't seem to be installed at {godasum_dir}");
			}

			var dest_dir = Path.Combine(godasum_dir, "mods", fullname, "mono");

			_copy(source_dir, dest_dir);
		}

		private static void _copy(string from, string to) {
			if (!Directory.Exists(from)) {
				throw new DirectoryNotFoundException($"Couldn't copy directory {from}");
			}

			Directory.CreateDirectory(to);

			var di = new DirectoryInfo(from);
			var dirs = di.GetDirectories();
			var files = di.GetFiles();

			foreach (var file in files) {
				file.CopyTo(Path.Combine(to, file.Name), overwrite: true);
			}

			foreach (var dir in dirs) {
				_copy(dir.Name, Path.Combine(to, dir.Name));
			}

		}

	}
}
