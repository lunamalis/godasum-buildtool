namespace GodasumBuildtool.Activities {
	using System;
	using System.IO;

	public static class DirRemover
	{

		public static void Remove(string path) {

			if (!Directory.Exists(path)) {
				// No need to warn about this.
				return;
			}

			var di = new DirectoryInfo(path);
			if (di.Attributes.HasFlag(FileAttributes.ReparsePoint)) {
				Console.WriteLine($"WARN: Directory {path} is a reparse point. Will not delete this.");
				return;
			}
			if (!di.Attributes.HasFlag(FileAttributes.Directory)) {
				Console.WriteLine($"WARN: Directory {path} is actually a file. Will not delete this.");
				return;
			}

			Console.WriteLine($"Deleting directory {path}");
			Directory.Delete(path, true);
		}

	}
}
