namespace GodasumBuildtool.Activities {
	using System;
	using System.IO;

	public static class GetGodasum
	{

		public static void Run(Buildtool bt) {

			var godasum_dll_path = bt.Config.GodasumPath;
			if (!File.Exists(godasum_dll_path)) {
				throw new FileNotFoundException($"Godasum doesn't seem to be installed at the given path of {godasum_dll_path}");
			}

			var dest_dir = Path.Combine(bt.TargetPath, ".mono", "lib", "Godasum");
			Directory.CreateDirectory(dest_dir);

			File.Copy(godasum_dll_path, Path.Combine(dest_dir, "Godasum.dll"));

		}
	}
}
