namespace GodasumBuildtool.Activities {

	using System;
	using System.IO;
	using System.Diagnostics;

	public static class GetGodotSharp
	{

		public static void Run(Buildtool bt) {

			var godot_dir = Directory.GetParent(bt.Config.GodotPath).FullName;
			var sharp_dll_path = Path.Combine(godot_dir, "GodotSharp", "Api", "Debug", "GodotSharp.dll");
			var sharpr_dll_path = Path.Combine(godot_dir, "GodotSharp", "Api", "Release", "GodotSharp.dll");
			var sharpe_dll_path = Path.Combine(godot_dir, "GodotSharp", "Api", "Debug", "GodotSharpEditor.dll");
			var sharper_dll_path = Path.Combine(godot_dir, "GodotSharp", "Api", "Release", "GodotSharpEditor.dll");
			var dest_dir = Path.Combine(bt.TargetPath, ".mono", "assemblies", "Debug");
			var destr_dir = Path.Combine(bt.TargetPath, ".mono", "assemblies", "Release");

			if (
				!File.Exists(sharp_dll_path)
				||
				!File.Exists(sharpr_dll_path)
				||
				!File.Exists(sharpe_dll_path)
				||
				!File.Exists(sharper_dll_path)
			) {
				throw new FileNotFoundException($"Godot doesn't seem to be installed at the given path of {godot_dir}");
			}

			Directory.CreateDirectory(dest_dir);
			Directory.CreateDirectory(destr_dir);

			File.Copy(sharp_dll_path, Path.Combine(dest_dir, "GodotSharp.dll"));
			File.Copy(sharpr_dll_path, Path.Combine(destr_dir, "GodotSharp.dll"));
			File.Copy(sharpe_dll_path, Path.Combine(dest_dir, "GodotSharpEditor.dll"));
			File.Copy(sharper_dll_path, Path.Combine(destr_dir, "GodotSharpEditor.dll"));

		}
	}
}
