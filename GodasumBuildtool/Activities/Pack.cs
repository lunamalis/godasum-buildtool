namespace GodasumBuildtool.Activities {

	using System;
	using System.IO;
	using System.Diagnostics;

	public static class Pack
	{
		// TODO: we could also possibly utilize this tool:
		// https://github.com/hhyyrylainen/GodotPckTool

		public static void Run(Buildtool bt) {

			var packer_path = _fetch_packer_script_path(bt.TargetPath, bt.DataPath);

			var godot_process = new Process();

			var start_info = godot_process.StartInfo;
			start_info.UseShellExecute = false;
			start_info.RedirectStandardOutput = true;
			start_info.RedirectStandardError = true;
			start_info.CreateNoWindow = true;

			// We have to set the work directory for Godot somewhere.
			// If we don't, it will write things like logs into the current
			// directory. Not good!
			var workdir = Path.Combine(bt.DataPath, "godotworkdir");
			Directory.CreateDirectory(workdir);
			start_info.WorkingDirectory = workdir;

			start_info.FileName = bt.Config.GodotPath;

			// The `-s` is to run a script.
			// The `--no-window` disables the window, which we don't need.
			// However, it only works on Windows, so we'll have to instruct
			// non-Windows users to either:
			// 1. Deal with an annoying window popping up
			// 2. Use the headless mono edition instead
			// WSL users: If you are getting errors about "invalid locale 'C'",
			// either set your locale in your bash profile to en_US.UTF-8 or
			// your choice of language.
			// We also add the target path afterwards; this argument is passed to
			// the script.
			// CLI arguments here:
			// https://docs.godotengine.org/en/stable/getting_started/editor/command_line_tutorial.html
			start_info.Arguments = $"-s {packer_path} --no-window {bt.TargetPath}";

			godot_process.Start();
			godot_process.WaitForExit();

			Console.WriteLine($"STDOUT:\n{godot_process.StandardOutput.ReadToEnd()}\n");
			Console.WriteLine($"STDERR:\n{godot_process.StandardError.ReadToEnd()}\n");

			// Now, we have a tmp directory with the file in it.
			var pck_source_path = Path.Combine(bt.TargetPath, "build", "tmp", "data.pck");
			if (!File.Exists(pck_source_path)) {
				throw new FileNotFoundException($"Couldn't find packed source data at {pck_source_path}");
			}

			var pck_dest_dir = Path.Combine(bt.TargetPath, "build", bt.ModInfo.FullName);
			Directory.CreateDirectory(pck_dest_dir);
			File.Move(pck_source_path, Path.Combine(pck_dest_dir, "data.pck"), overwrite: true);

			// Then copy modinfo
			var modinfo_source_path = Path.Combine(bt.TargetPath, "modinfo.json");
			var modinfo_dest_path = Path.Combine(bt.TargetPath, "build", bt.ModInfo.FullName, "modinfo.json");
			File.Copy(modinfo_source_path, modinfo_dest_path, overwrite: true);

		}

		private static string _fetch_packer_script_path(string target_path, string data_path) {
			var self_packer_path = Path.Combine(target_path, "pack.gd");
			if (File.Exists(self_packer_path)) {
				return self_packer_path;
			}

			var default_packer_path = Path.Combine(data_path, "defaultpack.gd");

			#if DEBUG
				if (File.Exists(default_packer_path)) {
					File.Delete(default_packer_path);
				}
			#endif

			if (!File.Exists(default_packer_path)) {
				// Generate it.
				var assembly = typeof(Buildtool).Assembly;
				string packer_text;
				using (var stream = assembly.GetManifestResourceStream("GodasumBuildtool.embed.DefaultPacker.gd")) {
					if (stream is null) {
						throw new InvalidOperationException("Embedded resource could not be loaded.");
					}

					using (var reader = new StreamReader(stream)) {
						packer_text = reader.ReadToEnd();
					}

				}

				File.WriteAllText(default_packer_path, packer_text);
			}

			return default_packer_path;

		}

	}
}
