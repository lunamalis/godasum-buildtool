namespace GodasumBuildtool {
	using System;
	using System.Linq;
	using System.IO;
	using System.Diagnostics;

	public class Buildtool
	{

		public Tasks.TaskCollection Tasks = new Tasks.TaskCollection() {
			new Tasks.BuildDLLDebug(),
			new Tasks.BuildDLLRelease(),
			new Tasks.Clean(),
			new Tasks.CleanTransfer(),
			new Tasks.ExportDebug(),
			new Tasks.ExportRelease(),
			new Tasks.GetGodasum(),
			new Tasks.GetGodotSharp(),
			new Tasks.Help(),
			new Tasks.Pack(),
			new Tasks.Transfer(),
			new Tasks.TransferData(),
			new Tasks.TransferDLL(),
			new Tasks.TransferDLLAllDebug(),
			new Tasks.TransferDLLAllRelease()
		};

		public string TargetPath { get; protected set; }

		/// <summary>
		/// If true, all tasks will be skipped.
		///
		/// Individual tasks can toggle this to true in order
		/// to skip further tasks.
		/// </summary>
		/// <value></value>
		public bool BreakTasks { get; set; } = false;

		public Config Config { get; protected set; }

		public ModInfo ModInfo { get; protected set; }

		public string DataPath { get; protected set; }

		protected string _TargetPath;

		public Buildtool() {
			foreach (var task in this.Tasks) {
				task.Buildtool = this;
			}
		}

		public virtual void Run(string[] args) {
			// The first argument should be a task.
			if (args.Length <= 0) {
				// TODO: show help.
				Console.WriteLine("No arguments.");
				this._PrintTasks();
				return;
			}

			var (target_directory, fixed_args) = this._CollectTarget(args);
			args = fixed_args;
			target_directory = Path.GetFullPath(target_directory);
			Console.WriteLine($"Target dir: {target_directory}");
			this.TargetPath = target_directory;

			this.Config = this._GetConfig();
			this.ModInfo = this._GetModInfo();

			// Re-check arguments.
			if (args.Length <= 0) {
				Console.WriteLine("No arguments");
				this._PrintTasks();
				return;
			}

			// Read args for tasks.
			args = args.Reverse().ToArray();
			var args_list = args.ToList();
			for (int i = args_list.Count - 1; i >= 0; i--) {
				var arg = args_list[i];

				Tasks.ITask? task = null;
				foreach (var t in this.Tasks) {
					if (t.Aliases.Contains(arg)) {
						task = t;
						break;
					}
				}

				if (task is null) {
					Console.WriteLine($"Couldn't find a task named `{arg}`.");
					break;
				}

				args_list.RemoveAt(i);
				args = args_list.ToArray();

				Console.WriteLine($"Running task {task.Aliases[0]}");
				task.Run(args);
				Console.WriteLine("Finished task.");

				if (this.BreakTasks) {
					break;
				}

			}

		}

		protected (string, string[]) _CollectTarget(string[] args) {
			Debug.Assert(args.Length > 0);

			// Is the first argument a task?
			var first_arg = args[0];

			foreach (var t in this.Tasks) {
				if (t.Aliases.Contains(first_arg)) {
					// The first argument is a task.
					// The target directory must be the current working directory.
					return (Directory.GetCurrentDirectory(), args);
				}
			}

			string? target_directory;
			// The first argument wasn't a task.
			// Is it a target directory?
			if (Directory.Exists(first_arg)) {
				// They must be specifying it as a directory.
				target_directory = first_arg;

				// Remove it from arguments.
				args = args.Skip(1).ToArray();
			}
			else {
				// The first argument wasn't a task or a directory.
				throw new ArgumentException("Invalid first argument. Either it wasn't a task, or the target directory doesn't exist.");
			}

			if (target_directory is null) {
				target_directory = Directory.GetCurrentDirectory();
			}

			return (target_directory, args);

		}

		protected virtual Config _GetConfig() {
			// Probably Create is unnecessary, oh well.
			var appdata_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);

			var buildtool_data_path = Path.Combine(appdata_path, "GodasumBuildtool");
			Directory.CreateDirectory(buildtool_data_path);
			this.DataPath = buildtool_data_path;

			var config_path = Path.Combine(buildtool_data_path, "config.json");

			if (File.Exists(config_path)) {
				return Config.Load(config_path);
			}

			// Prompt the user for a new config.
			var c = new Config();

			Console.WriteLine("Need Godasum.dll.");
			do {
				Console.Write("Godasum.dll path: ");
				c.GodasumPath = Console.ReadLine();
				if (File.Exists(c.GodasumPath)) {
					break;
				}

				Console.WriteLine("That file doesn't exist. Try again.");

			} while(true);

			Console.WriteLine("Need Godot Mono version 3.2.3.");
			Console.WriteLine("Download it from:");
			Console.WriteLine("https://godotengine.org/download/");
			Console.WriteLine("Make sure to get the Mono version with C# support.");
			Console.WriteLine("Enter the path to the executable file that would start Godot.");
			do {
				Console.Write("Godot executable path: ");
				c.GodotPath = Console.ReadLine();
				if (File.Exists(c.GodotPath)) {
					break;
				}

				Console.WriteLine("That file doesn't exist. Try again.");

			} while (true);

			Console.WriteLine("Need the .NET Core SDK tools.");
			Console.WriteLine("These tools are used to compile the C# part of your mod.");
			Console.WriteLine("You can read about it at:");
			Console.WriteLine("https://docs.microsoft.com/en-us/dotnet/core/tools/");
			Console.WriteLine("And install it from (choose your operating system):");
			Console.WriteLine("https://docs.microsoft.com/en-us/dotnet/core/install/");
			do {
				Console.Write("dotnet path: ");
				c.DotnetToolsPath = Console.ReadLine();
				if (File.Exists(c.DotnetToolsPath)) {
					break;
				}

				Console.WriteLine("That file doesn't exist. Try again.");

			} while (true);

			c.Save(config_path);

			return c;

		}

		protected virtual ModInfo _GetModInfo() {
			var modinfo_path = Path.Combine(this.TargetPath, "modinfo.json");
			if (!File.Exists(modinfo_path)) {
				throw new FileNotFoundException($"Couldn't find modinfo.json at {modinfo_path}");
			}

			var text = File.ReadAllText(modinfo_path);
			var mi = Newtonsoft.Json.JsonConvert.DeserializeObject<ModInfo>(text);
			return mi;

		}

		protected virtual void _PrintTasks() {
			foreach (var task in this.Tasks) {
				Console.WriteLine($"Task {task.Aliases[0]}:\n\t{task.Description}");
			}
		}

	}
}
