namespace GodasumBuildtool {
	using System.IO;
	using Newtonsoft.Json;

	[JsonObject]
	public class Config
	{

		public string GodasumPath { get; set; }
		public string GodotPath { get; set; }
		public string DotnetToolsPath { get; set; }

		public void Save(string filepath) {
			var text = JsonConvert.SerializeObject(this, Formatting.Indented);
			File.WriteAllText(filepath, text);
		}

		public static Config Load(string filepath) {
			var text = File.ReadAllText(filepath);
			return JsonConvert.DeserializeObject<Config>(text);
		}

	}
}
