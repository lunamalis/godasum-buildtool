namespace GodasumBuildtool {
	using Newtonsoft.Json;

	[JsonObject]
	public sealed class ModInfo
	{

		[JsonProperty("cname")]
		public string NamespaceName { get; private set; }

		[JsonProperty("version")]
		public string Version { get; private set; }

		[JsonProperty("loader_version")]
		public string LoaderVersion { get; private set; }

		[JsonIgnore]
		public string FullName {
			get {
				return $"{this.NamespaceName}-{this.LoaderVersion}-{this.Version}";
			}
		}

	}
}
