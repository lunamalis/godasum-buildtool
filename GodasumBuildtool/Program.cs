namespace GodasumBuildtool {
	using System;
	using System.IO;

	internal class Program
	{

		public static void Main(string[] args) {
			Console.WriteLine("Build tool start.");
			var bt = new Buildtool();

			// string target_directory;
			// if (args.Length <= 0) {
			// 	target_directory = Directory.GetCurrentDirectory();
			// }
			// else {
			// 	target_directory = args[0];
			// }

			// if (!Directory.Exists(target_directory)) {
			// 	Console.WriteLine($"Error: Target directory `{target_directory}` does not exist.");
			// 	return;
			// }

			// bt.Pack(target_directory);

			bt.Run(args);

		}

	}
}
