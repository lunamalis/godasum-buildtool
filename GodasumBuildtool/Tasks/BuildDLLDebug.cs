namespace GodasumBuildtool.Tasks {
	public class BuildDLLDebug : ITask
	{
		// worth taking a look at:
		// https://github.com/godotengine/godot/blob/master/modules/mono/editor/Godot.NET.Sdk/Godot.NET.Sdk/Godot.NET.Sdk.csproj

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "build-dll-debug" };

		public string Description => "Build DLL for the mod, under debug mode.";

		public void Run(string[] args) {
			Activities.BuildDLL.Run(this.Buildtool, "Debug");
		}

	}
}
