namespace GodasumBuildtool.Tasks {
	public class BuildDLLRelease : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "build-dll-release" };

		public string Description => "Build DLL for the mod, under release mode.";

		public void Run(string[] args) {
			Activities.BuildDLL.Run(this.Buildtool, "Release");
		}

	}
}
