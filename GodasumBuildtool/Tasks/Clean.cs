namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class Clean : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "clean" };

		public string Description => "Clean up a build repository.";

		public void Run(string[] args) {
			var dirs_to_remove = new[] {
				// Don't delete this directory.
				// It contains some stuff populated by Godot.
				".mono",
				"build"
			};

			foreach (var dir in dirs_to_remove) {
				var full_dir_path = Path.Combine(this.Buildtool.TargetPath, dir);

				Activities.DirRemover.Remove(full_dir_path);

			}

		}

	}
}
