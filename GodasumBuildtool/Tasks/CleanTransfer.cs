namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class CleanTransfer : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "clean-transfer" };

		public string Description => "Clean transferred mod in Godasum mods folder.";

		public void Run(string[] args) {
			var appdata_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);

			var godasum_dir = Path.Combine(appdata_path, "Godasum");
			if (!Directory.Exists(godasum_dir)) {
				throw new DirectoryNotFoundException($"Godasum doesn't seem to be installed at {godasum_dir}");
			}

			var mi = this.Buildtool.ModInfo;

			var mod_dir = Path.Combine(godasum_dir, "mods", this.Buildtool.ModInfo.FullName);

			Activities.DirRemover.Remove(mod_dir);

		}
	}
}
