namespace GodasumBuildtool.Tasks {
	public class ExportDebug : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "export-debug" };

		public string Description => "Export the mod, under debug mode.\n\tThis builds the DLL and packages data.";

		public void Run(string[] args) {
			var p = new Pack();
			p.Buildtool = this.Buildtool;

			// TODO: show args or not?
			p.Run(args);

			var b = new BuildDLLDebug();
			b.Buildtool = this.Buildtool;
			b.Run(args);

			var t = new TransferDLLAllDebug();
			t.Buildtool = this.Buildtool;
			t.Run(args);

			var td = new TransferData();
			td.Buildtool = this.Buildtool;
			td.Run(args);

		}

	}
}
