namespace GodasumBuildtool.Tasks {
	public class ExportRelease : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "export-release" };

		public string Description => "Export the mod, under release mode.\n\tThis will build the DLL, and package data.";

		public void Run(string[] args) {
			var p = new Pack();
			p.Buildtool = this.Buildtool;

			// TODO: show args or not?
			p.Run(args);

			var b = new BuildDLLRelease();
			b.Buildtool = this.Buildtool;
			b.Run(args);

			var t = new TransferDLLAllDebug();
			t.Buildtool = this.Buildtool;
			t.Run(args);

			var td = new TransferData();
			td.Buildtool = this.Buildtool;
			td.Run(args);
		}

	}
}
