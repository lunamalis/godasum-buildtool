namespace GodasumBuildtool.Tasks {
	public class GetGodasum : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "get-godasum" };

		public string Description => "Get the Godasum DLL and put it into your mod's .mono/lib/Godasum/Godasum.dll folder.";

		public void Run(string[] args) {
			Activities.GetGodasum.Run(this.Buildtool);
		}

	}
}
