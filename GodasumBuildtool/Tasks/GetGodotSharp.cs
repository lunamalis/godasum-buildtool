namespace GodasumBuildtool.Tasks {
	using System;

	public class GetGodotSharp : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "get-godot-sharp" };

		public string Description => "Get Godot assemblies.";

		public void Run(string[] args) {
			Activities.GetGodotSharp.Run(this.Buildtool);
		}

	}
}
