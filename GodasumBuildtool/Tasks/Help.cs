namespace GodasumBuildtool.Tasks {
	using System;

	public sealed class Help : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "help" };

		public string Description => "Get help with a task.";

		public void Run(string[] args) {
			this.Buildtool.BreakTasks = true;

			if (args.Length <= 0) {
				Console.WriteLine("buildtool [TARGET DIRECTORY] help <task name>");
				return;
			}

			if (args.Length > 1) {
				throw new ArgumentException("Help task can only help with one task at a time.");
			}

			var task_name = args[0];
			var task = this.Buildtool.Tasks.GetTask(task_name);

			Console.WriteLine($"Task \"{task.Aliases[0]}\":\n\t{task.Description}");

		}
	}
}
