namespace GodasumBuildtool.Tasks {
	/// <summary>
	/// Handles input/output, with <see cref="Activities"/> doing most of the work.
	/// </summary>
	public interface ITask
	{

		/// <summary>
		/// The values that the task is named as.
		/// Can have multiple values.
		/// </summary>
		/// <value>Names.</value>
		string[] Aliases { get; }

		/// <summary>
		/// The description of what this task does.
		/// </summary>
		/// <value></value>
		string Description { get; }

		/// <summary>
		/// The parent <see cref="Buildtool"/>.
		///
		/// Automatically assigned.
		/// </summary>
		/// <value>Parent build tool.</value>
		Buildtool Buildtool { get; set; }

		/// <summary>
		/// Run the task.
		/// </summary>
		/// <param name="args">Arguments. May contain further tasks.</param>
		void Run(string[] args);

	}
}
