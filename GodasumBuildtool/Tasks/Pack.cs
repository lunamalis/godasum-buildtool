namespace GodasumBuildtool.Tasks {
	public class Pack : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "pack" };

		public string Description => "Pack mod data to a .pck file.";

		public void Run(string[] args) {
			Activities.Pack.Run(this.Buildtool);
		}

	}
}
