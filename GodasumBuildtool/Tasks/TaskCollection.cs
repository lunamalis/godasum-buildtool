namespace GodasumBuildtool.Tasks {
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using System.Collections.ObjectModel;

	public class TaskCollection : IEnumerable<ITask>
	{

		public ReadOnlyCollection<ITask> Tasks { get; set; }

		public int Count => this._Tasks.Count;

		protected List<ITask> _Tasks { get; set; } = new List<ITask>();

		public TaskCollection() {
			this.Tasks = new ReadOnlyCollection<ITask>(this._Tasks);
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this._Tasks.GetEnumerator();
		}

		IEnumerator<ITask> IEnumerable<ITask>.GetEnumerator() {
			return this._Tasks.GetEnumerator();
		}

		public void Add(ITask task) {
			this._Tasks.Add(task);
		}

		public bool ContainsAlias(string alias) {
			foreach (var task in this._Tasks) {
				if (task.Aliases.Contains(alias)) {
					return true;
				}
			}

			return false;
		}

		public ITask GetTask(string alias) {
			foreach (var task in this._Tasks) {
				if (task.Aliases.Contains(alias)) {
					return task;
				}
			}

			throw new ArgumentException($"Couldn't find a task with the alias `{alias}`.");
		}

	}
}
