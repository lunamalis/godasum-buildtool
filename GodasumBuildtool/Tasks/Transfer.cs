namespace GodasumBuildtool.Tasks {
	public class Transfer : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "transfer" };

		public string Description => "Transfer mod to Godasum mods folder.";

		public void Run(string[] args) {
			var td = new TransferData();
			td.Buildtool = this.Buildtool;
			td.Run(args);

			var tdl = new TransferDLL();
			tdl.Buildtool = this.Buildtool;
			tdl.Run(args);

			var tdla = new TransferDLLAllDebug();
			tdla.Buildtool = this.Buildtool;
			tdla.Run(args);

		}

	}
}
