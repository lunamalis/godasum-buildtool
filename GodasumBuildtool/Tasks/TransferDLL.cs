namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class TransferDLL : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "transfer-dll" };

		public string Description => "Transfer mod DLL to Godasum mods folder.";

		public void Run(string[] args) {
			var fullname = this.Buildtool.ModInfo.FullName;

			var source_file = Path.Combine(this.Buildtool.TargetPath, "build", fullname, "mono", $"{this.Buildtool.ModInfo.NamespaceName}.dll");
			if (!File.Exists(source_file)) {
				throw new FileNotFoundException($"Could not transfer mod DLL as source DLL file doesn't exist at {source_file}");
			}

			var appdata_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);

			var godasum_dir = Path.Combine(appdata_path, "Godasum");
			if (!Directory.Exists(godasum_dir)) {
				throw new DirectoryNotFoundException($"Godasum doesn't seem to be installed at {godasum_dir}");
			}

			var dest_dir = Path.Combine(godasum_dir, "mods", fullname, "mono");
			var dest_file = Path.Combine(dest_dir, $"{this.Buildtool.ModInfo.NamespaceName}.dll");
			Directory.CreateDirectory(dest_dir);

			File.Copy(source_file, dest_file, overwrite: true);

		}

	}
}
