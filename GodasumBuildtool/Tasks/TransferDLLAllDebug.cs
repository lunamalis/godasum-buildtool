namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class TransferDLLAllDebug : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "transfer-dll-all-debug" };

		public string Description => "Transfer all debug DLL dependencies to the mod folder, as well as your mod's debug DLL.";

		public void Run(string[] args) {
			Activities.DLLDependency.GetAll(this.Buildtool, "Debug");

		}

	}
}
