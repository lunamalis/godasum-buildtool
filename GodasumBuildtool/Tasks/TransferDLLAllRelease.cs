namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class TransferDLLAllRelease : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "transfer-dll-all-release" };

		public string Description => "Transfer all release DLL dependencies to the mod folder, as well as your mod's release DLL.";

		public void Run(string[] args) {
			Activities.DLLDependency.GetAll(this.Buildtool, "Release");

		}

	}
}
