namespace GodasumBuildtool.Tasks {
	using System;
	using System.IO;

	public class TransferData : ITask
	{

		public Buildtool Buildtool { get; set; }

		public string[] Aliases => new[] { "transfer-data" };

		public string Description => "Transfer mod packed data to Godasum mods folder.";

		public void Run(string[] args) {
			var fullname = this.Buildtool.ModInfo.FullName;

			var source_dir = Path.Combine(this.Buildtool.TargetPath, "build", fullname);
			if (!Directory.Exists(source_dir)) {
				throw new DirectoryNotFoundException($"Could not transfer mod data as source directory doesn't exist at {source_dir}");
			}

			var appdata_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);

			var godasum_dir = Path.Combine(appdata_path, "Godasum");
			if (!Directory.Exists(godasum_dir)) {
				throw new DirectoryNotFoundException($"Godasum doesn't seem to be installed at {godasum_dir}");
			}

			var source_file = Path.Combine(source_dir, "data.pck");

			var dest_dir = Path.Combine(godasum_dir, "mods", fullname);
			var dest_file = Path.Combine(dest_dir, "data.pck");
			Directory.CreateDirectory(dest_dir);

			File.Copy(source_file, dest_file, overwrite: true);

			// Also copy modinfo
			var modinfo_source_path = Path.Combine(source_dir, "modinfo.json");
			var modinfo_dest_path = Path.Combine(dest_dir, "modinfo.json");
			File.Copy(modinfo_source_path, modinfo_dest_path, overwrite: true);

		}

	}
}
