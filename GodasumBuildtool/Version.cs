namespace GodasumBuildtool {
	public static class Version
	{

		public static readonly string VERSION = "0.0.2";

		public static readonly string FLAVOR = "vanilla";

	}
}
