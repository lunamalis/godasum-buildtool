# Godasum Build Tool

This is used to build mods for Godasum.

## Usage

`./godasum-buildtool.exe [PATH TO MOD DIRECTORY] <TASK>`

The first argument should be the path to the mod directory, but is optional.
All further arguments should be task names. Provide no arguments to see the list
of available tasks.

You only need to install this tool once. It can be installed anywhere on a machine.
You can optionally put it in your PATH to make it easier to reference.

When using this tool with a mod, you can run the `clean` task to reset to an
initial state. You can run the `clean-transfer` task to reset the "transferred" mod.

When a mod is built, it is built inside of the `build` directory. "Transferring"
means to copy the built data to Godasum's mod directory. To seee the result of a
change, you must transfer after every change.

### First Steps

When starting with a mod from scratch, you should run the
`get-godasum get-godot-sharp` tasks. This will fetch the necessary DLLs required
for you to compile your mod. You only need to do this once per mod, but
you'll have to do it again if you ever run the `clean` task.

Then, use `export-debug` to export your mod. *Some*, but not all of the product will
be found in the newly created `build` folder. All of it will be found in Godasum's
mods folder.

When you're ready to release it to the public, `export-release` will export it under
release mode.

### Extra

Running the export task takes a while, doesn't it? Wouldn't it be great if it could
be faster? It builds your entire mod every time you run it, even if you change one
thing.

Well, you can run other, simpler tasks to be more specific in what you
want done.

`build-dll-debug` will build your mod's DLL file, and copy it to
your build folder. Then, `transfer-dll` will transfer your DLL file to Godasum's mods folder. You could run `godasum-buildtool.exe build-dll-debug transfer-dll` in order to quickly build
just the script part of your mod.

**Note**: if you have any dependencies of your mod, you should run
`transfer-dll-all-debug` to transfer dependencies as well. However, you only
need to do this when you change or add dependencies.

**Note**: you can change "debug" to "release" if you want to build in release mode.

Similarly, you can run `pack` to pack the Godot data of your mod, and then
`transfer-data` to transfer that to the mods directory.

Do not zip up the folder in your build directory for distribution. Instead, zip up
the mod's folder in Godasum's mods folder.

### Tasks

`build-dll-debug`

Build your mod's DLL file under debug mode, and copy it to the build folder.

`build-dll-release`

Like `build-dll-debug`, except its under release mode.

`clean`

Clean your mod repository, resetting it to a more initial state.

This will delete `.mono` and `build`.

`clean-transfer`

Delete any transferred mod files in Godasum's mods folder.

`export-debug`

Export your entire mod to Godasum's mods folder under debug mode.

`export-release`

Like `export-debug`, except its under release mode.

`get-godasum`

Get Godasum's DLL and put it at `.mono/lib/Godasum/Godasum.dll`.

`get-godot-sharp`

Get GodotSharp's DLLs and put it somewhere in `.mono/assemblies/`.

`help`

Get help on a specific task. May not offer much help.

`pack`

Pack your mod's source folder and the `resources` folder into a `data.pck` file.

`transfer`

Runs `transfer-data`, `transfer-dll`, and `transfer-dll-all-debug`.

`transfer-data`

Copies your mod's `data.pck` to Godasum's mods folder.

You should run `pack` before running this.

`transfer-dll`

Copies your mod's DLL to Godasum's mods folder.

You should run `build-dll-debug` or `build-dll-release` before running this.

`transfer-dll-all-debug`

Copies all DLLs inside of `.mono/temp/bin/Debug` to your build directory,
*and* Godasum's mods folder.

`transfer-dll-all-release`

Like `transfer-dll-all-debug`, except its under release mode, and the path
is `.mono/temp/bin/Release`.

### Symbolic Links

The `clean` task will avoid attempting to delete symbolic links (symlinks).

You can attempt to symlink your build directory and your `.mono/temp/bin/<config>`
directory, if you choose. This can allow you to skip "transferring" as the operating
system will do it for you.

## Building

Uses the .NET Core SDK dev tools. Build with `dotnet build` and run with `dotnet run`.

The output is stored in `build/bin/<config>/netcoreapp3.1/`.
