#!/usr/bin/env -S godot -s
extends SceneTree

# Hey! If you decide to edit this file from the default,
# please change the print statement in the run function.

func _init():
	run()
	quit()

func run():
	print("Default script started.")
	# print("Script started.")

	# The arguments length should always be 3.
	# The first argument should be `-s`.
	# The second argument should be the path to the packer script (this file).
	# The third argument is the interesting one; it should point to the
	# directory we need to target.
	var args = OS.get_cmdline_args()
	if len(args) != 3:
		push_error("Incorrect number of arguments.")
		quit()
		return

	var target_dir = args[2]
	if not target_dir.ends_with("/"):
		target_dir = target_dir + "/"
	print("Targeting: " + target_dir)

	if not _dir_exists(target_dir):
		push_error("Target directory doesn't exist.")
		quit()
		return

	# Find modinfo.json. This contains some useful information,
	# like the mod name.
	var modinfo_path = target_dir + "modinfo.json";
	if not _file_exists(modinfo_path):
		push_error("modinfo.json doesn't exist in target folder at %s" % modinfo_path)
		quit()
		return

	var modinfo = _read_json_file(modinfo_path)
	print("Packing %s" % modinfo["cname"])

	var namespace_path = target_dir + modinfo["cname"]
	if not _dir_exists(namespace_path):
		push_error("Couldn't find source directory at %s" % namespace_path)

	# Just need the files, not the directories.
	var namespace_files = walk_dir(namespace_path)[1]

	var files_to_pack = []
	for file in namespace_files:
		if not file.ends_with(".tscn"):
			continue

		files_to_pack.append(file)

	var resource_path = target_dir + "resources"
	# The resources directory is optional.
	if _dir_exists(resource_path):
		var resource_files = walk_dir(resource_path)[1]
		files_to_pack += resource_files

	# if len(files_to_pack) <= 0:
	# 	print("There are no files to pack. Quitting.")
	# 	quit()
	# 	return

	var build_dir = target_dir + "build"
	var build_temp_dir = build_dir + "/tmp"
	var pack_destination = build_temp_dir + "/data.pck"

	_mkdir_if_needed(build_dir)
	_mkdir_if_needed(build_temp_dir)

	print("Packing %s files..." % len(files_to_pack))
	var begin = OS.get_system_time_msecs()
	_pack_source(pack_destination, target_dir, files_to_pack)
	var now = OS.get_system_time_msecs()
	print("Finished packing in %s seconds." % ((now - begin) / 1000.0))

func walk_dir(root: String) -> Array:
	var directories = []
	var files = []

	var directory = Directory.new()

	if directory.open(root) == OK:
		directory.list_dir_begin(true, false)
		_walk_dir(directory, directories, files, root.get_file() + "/")
		directory.list_dir_end()
	else:
		print("Couldn't read directory.")

	return [directories, files]


func _walk_dir(dir: Directory, directories: Array, files: Array, descent: String):
	var filename = dir.get_next()

	while (filename != ""):
		var path = dir.get_current_dir() + "/" + filename

		if dir.current_is_dir():
			var child_dir = Directory.new()
			child_dir.open(path)

			child_dir.list_dir_begin(true, false)
			directories.append(child_dir)
			_walk_dir(child_dir, directories, files, descent + filename + "/")
			child_dir.list_dir_end()

		else:
			files.append(descent + filename)

		filename = dir.get_next()
	# end

func _file_exists(path: String) -> bool:
	var file = File.new()
	var returner = file.file_exists(path)
	return returner

func _dir_exists(path: String) -> bool:
	var dir = Directory.new()
	var returner = dir.dir_exists(path)
	return returner

func _mkdir_if_needed(path: String):
	var dir = Directory.new()
	if dir.dir_exists(path):
		return

	var e = dir.make_dir(path)
	if e != OK:
		push_error("Error making directory: %s" % e)

func _read_file(path: String) -> String:
	var file = File.new()
	file.open(path, File.READ)
	var text = file.get_as_text()
	file.close()
	return text

func _read_json_file(path: String) -> Dictionary:
	var text = _read_file(path)
	var result = JSON.parse(text)

	if result.error == OK:
		return result.result

	push_error("Invalid JSON at " + path + ", line: " + str(result.error_line) + ", e: " + result.error_string)
	quit()
	# This shouldn't really be necessary, but it is.
	return {}

func _pack_source(destination: String, root: String, files: Array):
	var packer = PCKPacker.new()
	packer.pck_start(destination)

	for file in files:
		packer.add_file("res://%s" % file, root + "/" + file)

	packer.flush()
